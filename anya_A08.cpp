// source: https://www.tutorialspoint.com/cplusplus-program-to-implement-the-vigenere-cypher
#include <iostream>
#include <string>
#include <cstring>
#include <ctype.h>
#include <fstream>

using namespace std;

class Vig {
 private:
      string k;
      string sEncryption;
      string sDecryption;
      string withIan;

 public:
   Vig(string k) {
      for (int i = 0; i < k.size(); ++i) {
         if (k[i] >= 'A' && k[i] <= 'Z')
            this->k += k[i];
         else if (k[i] >= 'a' && k[i] <= 'z')
            this->k += k[i] + 'A' - 'a';
      }
   }
   string encryption(string t) {
      string output;
      for (int i = 0, j = 0; i < t.length(); ++i) {
         char c = t[i];
		 char temp = t[i];
         if (c >= 'a' && c <= 'z')
            c += 'A' - 'a';
         else if (c < 'A' || c > 'Z')
            continue;
		if (islower(temp))
			output += (c + k[j] - 2 * 'A') % 26+ 'a';
		else
			output += (c + k[j] - 2 * 'A') % 26+ 'A';
         j = (j + 1) % k.length();
      }
      return output;
   }
   string decryption(string t) {
      string output;
      for (int i = 0, j = 0; i < t.length(); ++i) {
         char c = t[i];
		 char temp = t[i];
         if (c >= 'a' && c <= 'z')
            c += 'A' - 'a';
         else if (c < 'A' || c > 'Z')
            continue;
		
		if (islower(temp))
			output += (c - k[j] + 26) % 26 + 'a';
		else
			output += (c - k[j] + 26) % 26 + 'A';

        j = (j + 1) % k.length();
      }
      return output;
   }

   string addingDot(string ori, string t){
	   for (int i = 0; i < ori.length(); ++i){
		   //  char c = ori[i];
          string c2="";
          c2 += ori[i];
			if(!((c2 >= "a" && c2 <= "z") || (c2 >= "A" && c2 <= "Z")))
				t.insert(i, c2);
	   }
	   return t;
   }

   void proceedEncDec(string ori){
      string resultEnc="";
      string resultDec="";
      this->sEncryption="";
      this->sDecryption="";
      // string foreignChar = "";
      string word = "";
      for(int i=0; i< ori.length(); i++){
         char c = ori[i];
         if(c >= 'a' && c <= 'z' || (c >= 'A' && c <= 'Z')){
            word+=c;
         } else{
            if(word!=""){
               resultEnc+=encryption(word);
               resultDec+=decryption(word);
            }
            resultEnc+=c;
            resultDec+=c;
            word = "";
         }
         // cout<<word<<endl;
         if(i==ori.length()-1 && word !=""){
            resultEnc+=encryption(word);
            resultDec+=decryption(word);
         }
      }
      this->sEncryption = resultEnc;
      this->sDecryption = resultDec;
   }

   string get_sEncryption(){
      return this->sEncryption;
   }
   string get_sDecryption(){
      return this->sDecryption;
   }
   int get_last_slice(string ori){
      int index=-1;
      
      for(int i=0; i< ori.length(); i++){
         char c = ori[i];
         if(c=='/'){
            index=i;
         }
      }
      return index;
   }
   string get_ian(string ori){
      this->withIan = "";
      int index=get_last_slice(ori);
      this->withIan = ori.substr(0, index);
      string temp=ori.substr(index+1, ori.length());
      this->withIan+="/IAN_";
      this->withIan+=temp;
      return this->withIan;
   }
   string get_non_ian(string ori){
      this->withIan = "";
      int index=get_last_slice(ori);
      this->withIan = ori.substr(0, index);
      string temp=ori.substr(index+1, ori.length());
      this->withIan+="/IAN_";
      this->withIan+=temp;
      return this->withIan;
   }
   // setter
   void set_parentDir(){
      
   }
};

int main(int argc, char *argv[]) {
   Vig v("INNUGANTENG");
   string ori ="BillieEilish/idontwannabeyouanymore.txt";
   // cout<<"MASUK???"<<endl;
   v.proceedEncDec(ori);

   string source="";
   string dest="";
   string line="";

   source+=argv[1];
   dest+=argv[2];
   string pattern="";
   if(argv[3] != NULL || argv[3]!="")
      pattern+=argv[3];
   ifstream infile(source);
   fstream outfile(dest);
   if(!infile){
      cout<<"BROKEN"<<endl;
   }
   if(!infile){
      cout<<"BROKEN"<<endl;
   }
   bool isPatt=false;
   while (infile >> line)
   {
      // cout<<line<<endl;
      if(strstr(line.c_str(), "IAN_") && pattern != "" ){
         // cout<<"ini line awal "<<line<<endl;
         int index=pattern.length();
         // cout<<index<<" "<<line.length()<<endl;
         line=line.substr(index, line.length());
         isPatt=true;
         // cout<<"ini line akhir "<<line<<endl;
      } else{
         isPatt=false;
      }
      v.proceedEncDec(line);
      // process which to delete
      string oEnc, oDec,oIan;
      oEnc=v.get_sEncryption();
      oDec=v.get_sDecryption();
      if(!isPatt)
         oIan=v.get_ian(line);
      if(isPatt){
         string temp = pattern;
         temp.append(oEnc);
         oEnc=temp;
         temp = pattern;
         temp.append(oDec);
         oDec=temp;
         temp = pattern;
         temp.append(oIan);
         oIan=temp;
         temp = pattern;
         temp.append(line);
         line=temp;
      }
      string nonIan=line;
      string temp=line;
      if(strstr(line.c_str(), "IAN_")){
         int findIanInd=line.find("IAN_");
         nonIan=line.erase(findIanInd, 4);
         line = temp;
         oIan="kosong";
      }else{
         nonIan="kosong";         
      }
      // jika didecrypsinya demikian buat cek remove dari ian ke non ian
      if(strstr(oDec.c_str(), "IAN_")){
         int findIanInd=oDec.find("IAN_");
         oDec=oDec.erase(findIanInd, 4);
      }
      // buat last condition cek decrpsi sama kayak enc tpi non ian
      string toCekNonEncr=line;
      if(strstr(toCekNonEncr.c_str(), "IAN_")){
         int findIanInd=toCekNonEncr.find("IAN_");
         toCekNonEncr=toCekNonEncr.erase(findIanInd, 4);
      }else{
         toCekNonEncr="kosong";
      }

      outfile  << line << ";" 
               << oEnc << ";" 
               << oDec << ";" 
               << oIan << ";"
               << nonIan << ";"
               << toCekNonEncr << endl;
   }

   infile.close();
   outfile.close();

   cout << "Original Message: "<< ori << endl;
   cout << "Encrypted Message: " << v.get_sEncryption() << endl;
   cout << "Decrypted Message: " << v.get_sDecryption() << endl;
}